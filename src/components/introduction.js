import React, { Component } from 'react'
import Axios from 'axios';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    alignContent:'center',
    alignItems:'center'
  },
  textStyle:{

  }
}));




export default class Introduction extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      isLoading: true
    }
   
  }


  componentDidMount() {
    var params = {
      access_token: "dd0df21c8af5d929dff19f74506c4a8153d7acd34306b9761fd4a57cfa1d483c"
    }
    Axios.get("http://gisapi-web-staging-1636833739.eu-west-1.elb.amazonaws.com/v2/opportunities", { params }
    )
      .then(res => {

        this.setState({
          data: res.data.data,
          isLoading: false
        })
      })
      .catch(err => {
        console.log("Some problem with network ");
        this.setState({
          isLoading: false
        })
      })
  }



  render() {

    return (
      <div>
        <br/><br/><br/>
        {this.state.isLoading
          ?
          <div></div>
          :
          <div>
            
            <List style={{width:"100%",  textAlign:"center" }}>
              {this.state.data.map((item, index) => {
                return (  
                  <div key={index} style={{ paddingBottom: 8, paddingTop: 8 }}>
                    <span>
                      <h4>{item.title}</h4>

                    </span>
                    <Divider variant="inset" component="li" />


                  </div>
                )
              })}

            </List>


          </div>
        }
      </div>

    )






  }
}
