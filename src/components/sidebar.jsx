import React, { Component } from 'react'

export default class Sidebar extends Component {
  render() {
    return (
      <div>
        <div>
       
          <aside id="colorlib-aside" className="border js-fullheight">
            <div className="text-center">
            
            </div>
            <nav id="colorlib-main-menu" role="navigation" className="navbar">
              <div id="navbar" className="collapse">
                <ul>
                  <li className="active"><a href="#home" data-nav-section="home">List of Opportunities</a></li>
                </ul>
              </div>
            </nav>
        
       
          </aside>
        </div>
      </div>
    )
  }
}
