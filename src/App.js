import React, { Component } from 'react';
import './App.css';
import Sidebar from './components/sidebar'
import Introduction from './components/introduction.js'
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';


class App extends Component {
  render() {
    return (
      <div>
        <AppBar>
          <Toolbar>
            <Typography variant="h6">Scroll to Elevate App Bar</Typography>
          </Toolbar>
        </AppBar>
        <div id="colorlib-page">


          <div id="container-wrap">
            <Sidebar></Sidebar>
            <div id="colorlib-main">
              <Introduction></Introduction>


            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
